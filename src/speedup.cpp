
/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "speedup.h"
# define INF 9999

 //// Implemented functions. /////////////////////////////////////////////////

double  test(int threads, int iterations , double time_per_loop)
{
	long long int it_i;
	double counter;
	double start_time;

	//std::cout << "THREADS: " << threads << std::endl;
	//int threads = omp_get_max_threads();

	// Corre con hilos
	if(threads >= 2)
	{
		omp_set_num_threads( threads );
		start_time = omp_get_wtime();
		#pragma omp parallel for default(none) firstprivate(iterations, time_per_loop) private(it_i, counter)
		for(it_i=0; it_i<iterations; it_i++)
		{
			counter = omp_get_wtime();
			while(omp_get_wtime() - counter < time_per_loop);
		}
	}
	// Ejecucion lineal
	else
	{
		start_time = omp_get_wtime();
                for(it_i=0; it_i<iterations; it_i++)
                {
                        counter = omp_get_wtime();
                        while(omp_get_wtime() - counter < time_per_loop);
                }

	}
	//std::cout << "Time: " <<  omp_get_wtime() - start_time << std::endl;
	return omp_get_wtime() - start_time;
}

/*
dijkstra::dijkstra(std::string infile)
{
	this->V = 0;

	std::ifstream inData;
    inData.open(infile);
    if (!inData.good())
    {
        perror("Cannot open file");
        exit(-1);
    }

    inData >> this->V;
    std::cout << "Vertex: " << V << std::endl;
	this->G = std::vector< std::vector< int > >(V, std::vector<int>(V, INF) );

	for (int it_i = 0; it_i < V; ++it_i)
	{
		for (int it_j = 0; it_j < V; ++it_j)
		{
			inData >> this->G[it_i][it_j];
			this->G[it_i][it_j] = (this->G[it_i][it_j] == -1)? INF : this->G[it_i][it_j];
		}
	}
}


// Funtion that implements Dijkstra's single source shortest path algorithm
// for a graph represented using adjacency matrix representation
void dijkstra::calculate_distances(int threads)//int graph[V][V], int src)
{
	omp_set_num_threads(threads);
	std::cout << "\n\nsize of INT: " << sizeof(int) << "\n\n" << std::endl;

	struct timeval tim;
	int startnode = 0 ;
	int n = this->V;
	std::vector < std::vector< int > > cost(this->V, std::vector<int>(this->V));
	std::vector <int> distance(this->V);
	std::vector <int> pred(this->V);
	std::vector <int> visited(this->V);
	int count,  nextnode, i, j;

	//pred[] stores the predecessor of each node
	//count gives the number of nodes seen so far
	//create the cost matrix
    	for(i=0;i<n;i++)
        	for(j=0;j<n;j++)
            		if(this->G[i][j]==0)
                		cost[i][j]=INF;
            		else
                		cost[i][j]=this->G[i][j];

	for(i=0;i<n;i++)
	{
		distance[i] = cost[startnode][i];
		pred[i] = startnode;
        	visited[i]=0;
	}

	distance[startnode]=0;
	visited[startnode]=1;
	count=1;

	int id;
	int mindistance;
	int  global_min, global_vertex;
	int local_min, local_vertex;
	int it_i;
	int v_start, v_end;

	double start_time = omp_get_wtime();
	while(count<n-1)
	{
		mindistance = INF;
		#pragma omp parallel default(none) firstprivate(V, threads, distance, visited)  private(i, id, v_start, v_end, local_min, local_vertex, it_i) shared(global_min, global_vertex, std::cout)
        { 
			id = omp_get_thread_num();
			v_start = id * (V / threads);
                	v_end = v_start + (V / threads) - 1;
                	v_end = (v_end == V)? v_end-1:v_end;
			v_end = (id == threads-1)? V-1:v_end;

			#pragma omp single 
            {
                    global_min = INT_MAX; 
                    global_vertex = 0;
            }
            // Look for local min
            local_min = INT_MAX;
            for(it_i=v_start; it_i<=v_end; it_i++)
            {
                    if(!visited[it_i] && distance[it_i] < local_min)
                    {
                            local_min = distance[it_i];
                            local_vertex = it_i;
                    }
            }

            #pragma omp critical
            {
                    if(local_min < global_min)
                    {
                            global_min = local_min;
                            global_vertex = local_vertex;
                    }
			}
		}
		
		#pragma omp barrier
		nextnode = global_vertex;
		mindistance = global_min;
		
		visited[nextnode]=1;
		std::vector<int> aux = cost[nextnode];
		#pragma omp parallel for default(none) firstprivate(mindistance, visited, aux, nextnode, V) private(i) shared(distance, pred)
		for(it_i=0;it_i<V;it_i++)
            if(!visited[it_i])
				if(mindistance+aux[it_i]<distance[it_i])
				{
					distance[it_i]=mindistance+aux[it_i];
                    pred[it_i]=nextnode;
                }
        
        count++;
    	#pragma omp barrier
	}

	#pragma omp barrier

	long long int s_distances = 0;
	#pragma omp parallel for default(none) private(i) shared(distance, n) reduction(+:s_distances) 
	for(i=0;i<n;i++)
	{
		s_distances += distance[i];
	}
	std::cout << "Suma de distancias: " << s_distances << std::endl;
 
    	//gettimeofday(&tim, NULL);
    	//double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
	double time = omp_get_wtime() - start_time;

	std::cout << "\n\nOMP:" << std::endl;
	
	std::cout << "\n" << time << " seconds elapsed" <<  std::endl;
	
}
*/
