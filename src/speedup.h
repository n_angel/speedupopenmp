/*
 * DifferentialEvolution.h
 *
 */


#ifndef DIJKSTRA_H
#define DIJKSTRA_H


//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>
# include <vector>
# include <cstdio>
# include <map>
# include <fstream>
# include <climits>
# include <sys/time.h>

// OpenMP library
# include <omp.h>

//# define INF 2147483647


//// Global Vars	 //////////////////////////////////////////////////////// 
//// Used namespaces. ///////////////////////////////////////////////////////
//// New includes. //////////////////////////////////////////////////////////

double test(int threads, int iterations , double time_per_loop);

class speedup
{
/*
public:
	dijkstra(std::string infile);
	//~dijkstra();
	void calculate_distances(int threads);
	void printSolution(std::vector<long long int> dist);

private:
	int V;
	std::vector< std::vector<int> > G;

	int minDistance(std::vector<long long int> dist, std::vector<bool>);
*/
};

# endif
