// standard libraries
# include <iostream>
# include <cstdio>
# include <vector>
# include <random>
# include <algorithm>

// OpenMP library
# include <omp.h>
// new libraries
# include "parameter_handler.h"
//# include "dijkstra.h"
# include "speedup.h"

// Main
int main(int argc, char *argv[])
{
    //std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);
    //std::string infile = PARAMETER::get_value(params, "-infile");
    //int threads = std::stoi(PARAMETER::get_value(params, "-threads"));

	std::vector<double> TN;
	for (int it_its=8; it_its<8000001; it_its*=10)
	{
		std::cout << "Iterations: " << it_its << std::endl;
		for(double t_seg=0.1; t_seg<=1.0; t_seg+=0.1)
		{
			//std::vector<double> TN;
			//for(double t_seg=1.0; t_seg<=150.0; t_seg*=2.0)
			TN.clear();
			for(int thr=0; thr<12; thr+=2)
			{
				//std::cout << t_seg << "s -" << (thr==0?1:thr) << "thr\t";
				TN.push_back(test((thr==0?1:thr), it_its, t_seg));
				//T1 = (thr ==0? TN:T1);
				std::cout << TN[TN.size()-1] << "\t";
			}
			std::cout << std::endl;
			for(int it_i=0; it_i<TN.size(); it_i++)
			{
				std::cout << TN[0] / TN[it_i] << "\t";
			}
			std::cout << std::endl;
		}
		std::cout << "\n\n" << std::endl; 
	}

	std::cout << "Lineal: " << std::endl;
	test(1, 8, 1.0);

	int threads = 2;
	std::cout << threads << " threads" << std::endl;
	test(2, 8, 1.0);

    return 0;
}
